//////////////////////////////////////////////////////
//  droneMidLevelAutopilotROSModule.h
//
//  Created on: March 18, 2014
//      Author: jespestana
//
//  Last modification on: October 15, 2014
//      Author: hriday
//
//////////////////////////////////////////////////////


#ifndef _DRONE_ARUCO_EYE_ROS_MODULE_H
#define _DRONE_ARUCO_EYE_ROS_MODULE_H

// Define DRONE_DYNAMIC_TUNING in order to Tune PID Controller on the fly
//#define DRONE_DYNAMIC_TUNING

#include <iostream>
#include <string>
#include <vector>

// ROS
#include "ros/ros.h"
#include "droneModuleROS.h"
#include "DroneMidLevelAutopilot.h"
#include "communication_definition.h"
// ROS msgs
// Commands from quadrotor ROS architecture
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePose.h"
// Flying modes
#include "droneMsgsROS/droneStatus.h"
#include "droneMsgsROS/droneCommand.h"
// Measurement feedback
#include "geometry_msgs/Vector3Stamped.h" // IMU
#include "droneMsgsROS/droneAltitude.h"
#include "droneMsgsROS/vector2Stamped.h"  // Ground Optical Flow
// Commands to Pelican Autopilot
#include "droneMsgsROS/droneAutopilotCommand.h"

//dynamic reconfigure
#include <dynamic_reconfigure/server.h>
#include "droneMidLevelAutopilotROSModule/midlevelControllerConfig.h"

#include "nav_msgs/Odometry.h"

#include <eigen3/Eigen/Dense>

const double PELICAN_MID_LEVEL_AUTOPILOT_RATE = 30.0;
// #define VERBOSE_PELICAN_DMLA_ROS_MODULE

/////////////////////////////////////////
// Class DroneMidLevelAutopilotROSModule
//
//   Description
//
/////////////////////////////////////////
class DroneMidLevelAutopilotROSModule : public DroneModule
{
    //ArucoRetina
protected:
    DroneMidLevelAutopilot MyDroneMidLevelAutopilot;

public: // Constructors and destructors
    DroneMidLevelAutopilotROSModule();
    ~DroneMidLevelAutopilotROSModule();

private:
    std::string configFile;

public: // Init and close, related to Constructors and destructors
    void init();
    void close();
    void readParameters();
    bool readConfigs(std::string configFile);

protected: // DroneModule
    bool resetValues(); //Reset
    bool startVal(); //Start
    bool stopVal(); //Stop
public:
    bool run();     //Run
public: // Open, initialize subscribers and publishers
    void open(ros::NodeHandle & nIn, std::string moduleName);

 //Topic names
public:
    //Subscriber topic names
    std::string rotation_angles_topic_name;
    std::string sensor_altitude_topic_name;
    std::string ground_speed_topic_name;
    std::string command_pitch_roll_topic_name;
    std::string command_dYaw_topic_name;
    std::string command_dAltitude_topic_name;
    std::string command_high_level_topic_name;
    std::string estimated_speeds_topic_name;
    std::string estimated_pose_topic_name;

    //Publisher topic names
    std::string midlevel_status_topic_name;
    std::string command_low_level_topic_name;

    // Member variables
    double estimated_yaw_;

    // Subscribers
private:
    ros::Subscriber drone_rotation_angles_subscriber;
    ros::Subscriber drone_altitude_subscriber;
    ros::Subscriber drone_ground_optical_flow_subscriber;
    void droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg);
    void droneAltitudeCallback(const droneMsgsROS::droneAltitude& msg);
    void droneGroundOpticalFlowCallback(const droneMsgsROS::vector2Stamped& msg);


    ros::Subscriber drone_command_pitch_roll_subscriber;
    ros::Subscriber drone_command_dyaw_subscriber;
    ros::Subscriber drone_command_daltitude_subscriber;
    void droneDroneCommandPitchRollCallback(const droneMsgsROS::dronePitchRollCmd& msg);
    void droneDroneCommandDYawCallback(const droneMsgsROS::droneDYawCmd& msg);
    void droneDroneCommandDAltitudeCallback(const droneMsgsROS::droneDAltitudeCmd& msg);

    ros::Subscriber drone_flying_mode_command_subscriber;
    void flyingModeCommandCallback(const droneMsgsROS::droneCommand& msg);

    ros::Subscriber drone_estimated_speeds_subscriber;
    void droneEstimatedSpeedsCallback(const droneMsgsROS::droneSpeeds& msg);

    ros::Subscriber drone_estimated_pose_subscriber;
    void droneEstimatedPoseCallback(const droneMsgsROS::dronePose& msg);

    ros::Publisher drone_status_publisher;
    void publishDroneStatus();

    ros::Publisher drone_autopilot_command_publisher;
    bool publishDroneAutopilotCommand();

public:
    void parametersCallback(midlevelControllerROSModule::midlevelControllerConfig &config, uint32_t level);

};




#endif
