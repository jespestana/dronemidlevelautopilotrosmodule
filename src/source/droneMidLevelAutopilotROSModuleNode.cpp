//////////////////////////////////////////////////////
//  droneMidLevelAutopilotROSModuleNode.cpp
//
//  Created on: March 18, 2014
//      Author: jespestana
//
//  Last modification on: March 18, 2014
//      Author: jespestana
//
//////////////////////////////////////////////////////

#include <iostream>

//ROS
#include "ros/ros.h"
#include "droneMidLevelAutopilotROSModule.h"
#include "communication_definition.h"

int main(int argc,char **argv)
{
    // Ros Init
    ros::init(argc, argv, MODULE_NAME_DRONE_MIDLEVEL_AUTOPILOT);
    ros::NodeHandle n;
    std::cout << "[ROSNODE] Starting " << MODULE_NAME_DRONE_MIDLEVEL_AUTOPILOT << std::endl;

    // Vars
    DroneMidLevelAutopilotROSModule MyPelicanMidLevelAutopitot;
    MyPelicanMidLevelAutopitot.open(n,MODULE_NAME_DRONE_MIDLEVEL_AUTOPILOT);
  	

#ifdef DRONE_DYNAMIC_TUNING

    //dynamic Reconfigure
    dynamic_reconfigure::Server<midlevelControllerROSModule::midlevelControllerConfig> server;
    dynamic_reconfigure::Server<midlevelControllerROSModule::midlevelControllerConfig>::CallbackType f;

    f = boost::bind(&DroneMidLevelAutopilotROSModule::parametersCallback, &MyPelicanMidLevelAutopitot, _1, _2);
    server.setCallback(f);

#endif



	try
	{
        while(ros::ok())
		{
            //Read messages
            ros::spinOnce();

            //Run retina
            if(MyPelicanMidLevelAutopitot.run())
            {
            }

            //Sleep
            MyPelicanMidLevelAutopitot.sleep();
		}
        return 1;
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }
}
