 //////////////////////////////////////////////////////
//  droneMidLevelAutopilotROSModule.cpp
//
//  Created on: March 18, 2014
//      Author: jespestana
//
//  Last modification on: October 15, 2015
//      Author: hriday
//
//////////////////////////////////////////////////////


#include "droneMidLevelAutopilotROSModule.h"

////////// Drone Aruco Eye ///////////
DroneMidLevelAutopilotROSModule::DroneMidLevelAutopilotROSModule():
    DroneModule(droneModule::active,PELICAN_MID_LEVEL_AUTOPILOT_RATE),
    MyDroneMidLevelAutopilot(getId(), stackPath)
{
    std::cout << "DroneMidLevelAutopilotROSModule(...), enter and exit" << std::endl;
    return;
}


DroneMidLevelAutopilotROSModule::~DroneMidLevelAutopilotROSModule()
{
    close();
    return;
}

bool DroneMidLevelAutopilotROSModule::readConfigs(std::string configFile)
{
    try
    {
        XMLFileReader my_xml_reader(configFile);
        set_moduleRate(my_xml_reader.readDoubleValue("midlevel_autopilot_config:midlevel_autopilot:MODULE_FREQUENCY"));
    }
    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }

}

void DroneMidLevelAutopilotROSModule::init()
{
    readConfigs(stackPath+"configs/drone"+cvg_int_to_string(idDrone)+"/"+configFile);
    MyDroneMidLevelAutopilot.init(stackPath+"configs/drone"+cvg_int_to_string(idDrone)+"/"+configFile);
    return;
}


void DroneMidLevelAutopilotROSModule::close()
{
    if(!MyDroneMidLevelAutopilot.close())
        return;
    return;
}


//Reset
bool DroneMidLevelAutopilotROSModule::resetValues()
{
    if(!DroneModule::resetValues())
        return false;

    if(!MyDroneMidLevelAutopilot.reset())
        return false;

    return true;
}

//Start
bool DroneMidLevelAutopilotROSModule::startVal()
{
    if(!DroneModule::startVal())
        return false;

    if(!MyDroneMidLevelAutopilot.start())
        return false;

    return true;
}

//Stop
bool DroneMidLevelAutopilotROSModule::stopVal()
{
    if(!DroneModule::stopVal())
        return false;

    if(!MyDroneMidLevelAutopilot.stop())
        return false;

    return true;
}


//Run
bool DroneMidLevelAutopilotROSModule::run()
{
    if(!DroneModule::run()) {
        return false;
    }

    //Run Pelican Middle Level Autopilot
    if( !MyDroneMidLevelAutopilot.run() ) {
        return false;
    }

    //Publish
    publishDroneStatus();
    if(!publishDroneAutopilotCommand()) {
        return false;
    }

    //end
    return true;
}

void DroneMidLevelAutopilotROSModule::readParameters()
{
  ros::param::get("~config_file", configFile);
  if ( configFile.length() == 0)
  {
      configFile="midlevel_autopilot.xml";
  }

  //Subscriber names
  ros::param::get("~rotation_angles_topic_name", rotation_angles_topic_name);
  if (rotation_angles_topic_name.length() == 0)
  {
    rotation_angles_topic_name="rotation_angles";
  }

  ros::param::get("~sensor_altitude_topic_name", sensor_altitude_topic_name);
  if(sensor_altitude_topic_name.length() == 0)
  {
    sensor_altitude_topic_name="altitude";
  }

  ros::param::get("~ground_speed_topic_name", ground_speed_topic_name);
  if(ground_speed_topic_name.length() == 0)
  {
    ground_speed_topic_name="ground_speed";
  }

  ros::param::get("~command_pitch_roll_topic_name",command_pitch_roll_topic_name);
  if(command_pitch_roll_topic_name.length() == 0)
  {
    command_pitch_roll_topic_name="command/pitch_roll";
  }

  ros::param::get("~command_dYaw_topic_name",command_dYaw_topic_name);
  if(command_dYaw_topic_name.length() == 0)
  {
    command_dYaw_topic_name="command/dYaw";
  }

  ros::param::get("~command_dAltitude_topic_name",command_dAltitude_topic_name);
  if(command_dAltitude_topic_name.length() == 0)
  {
    command_dAltitude_topic_name="command/dAltitude";
  }

  ros::param::get("~command_high_level_topic_name",command_high_level_topic_name);
  if(command_high_level_topic_name.length() == 0)
  {
    command_high_level_topic_name="command/high_level";
  }

  ros::param::get("~estimated_speeds_topic_name",estimated_speeds_topic_name);
  if(estimated_speeds_topic_name.length() == 0)
  {
    estimated_speeds_topic_name="EstimatedSpeed_droneGMR_wrt_GFF";
  }

  ros::param::get("~estimated_pose_topic_name",estimated_pose_topic_name);
  if(estimated_pose_topic_name.length() == 0)
  {
    estimated_pose_topic_name="EstimatedPose_droneGMR_wrt_GFF";
  }

  //Publisher names
  ros::param::get("~midlevel_status_topic_name",midlevel_status_topic_name);
  if(midlevel_status_topic_name.length() == 0)
  {
    midlevel_status_topic_name="status";
  }

  ros::param::get("~command_low_level_topic_name",command_low_level_topic_name);
  if(command_low_level_topic_name.length() == 0)
  {
    command_low_level_topic_name="command/low_level";
  }


}

void DroneMidLevelAutopilotROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //read the topics
    readParameters();
    init();

    //configure droneArucoEye
    if(!MyDroneMidLevelAutopilot.configureAutopilot(stackPath+"configs/drone"+stringId+"/pelican_autopilot.xml"))
    {
#ifdef VERBOSE_PELICAN_DMLA_ROS_MODULE
        std::cout<<"[PelicanDMLA-ROS] Error reading configuration file"<<std::endl;
#endif
        return;
    }


    // Sensor Measurements
    drone_rotation_angles_subscriber     = n.subscribe(rotation_angles_topic_name, 1, &DroneMidLevelAutopilotROSModule::droneRotationAnglesCallback, this);
    drone_altitude_subscriber            = n.subscribe(sensor_altitude_topic_name, 1, &DroneMidLevelAutopilotROSModule::droneAltitudeCallback, this);
    drone_ground_optical_flow_subscriber = n.subscribe(ground_speed_topic_name, 1, &DroneMidLevelAutopilotROSModule::droneGroundOpticalFlowCallback, this);

    // Drone Middle Level Autopilot Commands
    drone_command_pitch_roll_subscriber = n.subscribe(command_pitch_roll_topic_name, 1, &DroneMidLevelAutopilotROSModule::droneDroneCommandPitchRollCallback, this);
    drone_command_dyaw_subscriber       = n.subscribe(command_dYaw_topic_name, 1, &DroneMidLevelAutopilotROSModule::droneDroneCommandDYawCallback, this);
    drone_command_daltitude_subscriber  = n.subscribe(command_dAltitude_topic_name, 1, &DroneMidLevelAutopilotROSModule::droneDroneCommandDAltitudeCallback, this);
    // Drone Flying Mode
    drone_flying_mode_command_subscriber = n.subscribe(command_high_level_topic_name, 1, &DroneMidLevelAutopilotROSModule::flyingModeCommandCallback, this);
    //EKF estimated speeds
    drone_estimated_speeds_subscriber       = n.subscribe(estimated_speeds_topic_name, 1, &DroneMidLevelAutopilotROSModule::droneEstimatedSpeedsCallback, this);
    drone_estimated_pose_subscriber       = n.subscribe(estimated_pose_topic_name, 1, &DroneMidLevelAutopilotROSModule::droneEstimatedPoseCallback, this);

    // Drone Flying/Status Mode
    drone_status_publisher = n.advertise<droneMsgsROS::droneStatus>(midlevel_status_topic_name, 1, true);

    // Drone Low Level Autopilot Commands
    drone_autopilot_command_publisher = n.advertise<droneMsgsROS::droneAutopilotCommand>(command_low_level_topic_name, 1, true);

    //Flag of module opened
    droneModuleOpened=true;

    //Autostart the module
    moduleStarted=true;
    MyDroneMidLevelAutopilot.start();

    //End
    return;
}

#ifdef DRONE_DYNAMIC_TUNING
void DroneMidLevelAutopilotROSModule::parametersCallback(midlevelControllerROSModule::midlevelControllerConfig &config, uint32_t level)
{
     MyDroneMidLevelAutopilot.setMidlevelDynamicGains(config.altitude_feedforward, config.altitude_kp, config.altitude_ki, config.altitude_kd);
}
#endif

void DroneMidLevelAutopilotROSModule::droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped &msg)
{
    MyDroneMidLevelAutopilot.setDroneMeasurementRotationAngles(msg.vector.x, msg.vector.y, msg.vector.z);
}

void DroneMidLevelAutopilotROSModule::droneAltitudeCallback(const droneMsgsROS::droneAltitude &msg)
{
    MyDroneMidLevelAutopilot.setDroneMeasurementAltitude( msg.altitude, msg.altitude_speed);
}

void DroneMidLevelAutopilotROSModule::droneGroundOpticalFlowCallback(const droneMsgsROS::vector2Stamped &msg)
{
    //Deprecated now using EKF estimated speeds
   // MyDroneMidLevelAutopilot.setDroneMeasurementGroundOpticalFlow(msg.vector.x, msg.vector.y);
}

void DroneMidLevelAutopilotROSModule::droneEstimatedSpeedsCallback(const droneMsgsROS::droneSpeeds &msg)
{
    //This was a bug, as the velocities to the midlevel should be in body frame and the estimated pose topic pubs in world frame
  double y, p, r;
  //     m.getEulerYPR(y, p, r);
  y = estimated_yaw_;
  p = 0;
  r = 0;

  Eigen::Vector3f BodyFrame;
  Eigen::Vector3f GlobalFrame;
  Eigen::Matrix3f RotationMat;

  GlobalFrame(0) = (+1)*msg.dx;
  GlobalFrame(1) = (+1)*msg.dy;
  GlobalFrame(2) = 0;

  RotationMat(0,0) = cos(y);
  RotationMat(1,0) = -sin(y);
  RotationMat(2,0) = 0;

  RotationMat(0,1) = sin(y);
  RotationMat(1,1) = cos(y);
  RotationMat(2,1) = 0;

  RotationMat(0,2) = 0;
  RotationMat(1,2) = 0;
  RotationMat(2,2) = 1;

  BodyFrame = RotationMat*GlobalFrame;

  MyDroneMidLevelAutopilot.setDroneMeasurementGroundOpticalFlow(BodyFrame(0), - BodyFrame(1));
}

void DroneMidLevelAutopilotROSModule::droneEstimatedPoseCallback(const droneMsgsROS::dronePose &msg)
{
  estimated_yaw_ = msg.yaw;
}

void DroneMidLevelAutopilotROSModule::droneDroneCommandPitchRollCallback(const droneMsgsROS::dronePitchRollCmd &msg)
{
    MyDroneMidLevelAutopilot.setAutopilotCommandData_PitchRoll( msg.pitchCmd,
                                                         msg.rollCmd);
}

void DroneMidLevelAutopilotROSModule::droneDroneCommandDYawCallback(const droneMsgsROS::droneDYawCmd &msg)
{
    MyDroneMidLevelAutopilot.setAutopilotCommandData_DYaw( msg.dYawCmd );
}

void DroneMidLevelAutopilotROSModule::droneDroneCommandDAltitudeCallback(const droneMsgsROS::droneDAltitudeCmd &msg)
{
    MyDroneMidLevelAutopilot.setAutopilotCommandData_DAltitude( msg.dAltitudeCmd );
}

void DroneMidLevelAutopilotROSModule::flyingModeCommandCallback(const droneMsgsROS::droneCommand &msg)
{
    DroneStateCommand::StateCommand last_drone_state_command = DroneStateCommand::IDLE;
    bool pass_command_to_midlevel_autopilot = true;
    switch (msg.command) {
    case droneMsgsROS::droneCommand::TAKE_OFF:
        last_drone_state_command = DroneStateCommand::TAKE_OFF;
        break;
    case droneMsgsROS::droneCommand::HOVER:
        last_drone_state_command = DroneStateCommand::HOVER;
        break;
    case droneMsgsROS::droneCommand::LAND:
        last_drone_state_command = DroneStateCommand::LAND;
        break;
    case droneMsgsROS::droneCommand::MOVE:
        last_drone_state_command = DroneStateCommand::MOVE;
        break;
    case droneMsgsROS::droneCommand::RESET:
    // case droneMsgsROS::droneCommand::EMERGENCY_STOP: // duplicate case value, same integer value
        last_drone_state_command = DroneStateCommand::EMERGENCY_STOP;
        break;
    case droneMsgsROS::droneCommand::IDLE:
    case droneMsgsROS::droneCommand::INIT:
    case droneMsgsROS::droneCommand::UNKNOWN:
    default:
        pass_command_to_midlevel_autopilot = false;
        break;
    }

    if (pass_command_to_midlevel_autopilot) {
        MyDroneMidLevelAutopilot.setDroneFlyingModeCommand(last_drone_state_command);
    }
}

void DroneMidLevelAutopilotROSModule::publishDroneStatus()
{
    DroneState::ModeType current_drone_status;
    MyDroneMidLevelAutopilot.getDroneFlyingMode( &current_drone_status );

    droneMsgsROS::droneStatus current_drone_status_msg;

    switch (current_drone_status) {
    case DroneState::UNKNOWN:
    // case DroneState::EMERGENCY: // duplicate case value (same integer value)
        current_drone_status_msg.status = droneMsgsROS::droneStatus::UNKNOWN;
        //        current_drone_status_msg.status = droneMsgsROS::droneStatus::EMERGENCY;
        break;
    case DroneState::FLYING:
        current_drone_status_msg.status = droneMsgsROS::droneStatus::FLYING;
        break;
    case DroneState::HOVERING:
        current_drone_status_msg.status = droneMsgsROS::droneStatus::HOVERING;
        break;
    case DroneState::INIT:
        current_drone_status_msg.status = droneMsgsROS::droneStatus::INITED;
        break;
    case DroneState::LANDED:
        current_drone_status_msg.status = droneMsgsROS::droneStatus::LANDED;
        break;
    case DroneState::LANDING:
        current_drone_status_msg.status = droneMsgsROS::droneStatus::LANDING;
        break;
    case DroneState::TAKING_OFF:
        current_drone_status_msg.status = droneMsgsROS::droneStatus::TAKING_OFF;
        break;
    case DroneState::LOOPING:
        current_drone_status_msg.status = droneMsgsROS::droneStatus::LOOPING;
        break;
    default:
        current_drone_status_msg.status = droneMsgsROS::droneStatus::UNKNOWN;
        break;
    }

    drone_status_publisher.publish( current_drone_status_msg );
}

bool DroneMidLevelAutopilotROSModule::publishDroneAutopilotCommand()
{
    DroneState::ModeType current_drone_mode = MyDroneMidLevelAutopilot.getDroneMLAutopilotStatus();

    switch (current_drone_mode) {
    case DroneState::INIT:
    case DroneState::LOOPING:
#ifndef EMERGENCY_MODE_ENABLE
    case DroneState::EMERGENCY:
    // case DroneState::UNKNOWN:
#endif
    {
        // Do not publish a CtrlInput command
        return false;
    } break;
    case DroneState::LANDED:
    case DroneState::TAKING_OFF:
    case DroneState::HOVERING:
    case DroneState::FLYING:
    case DroneState::LANDING:
#ifdef EMERGENCY_MODE_ENABLE
    case DroneState::EMERGENCY:
    // case DroneState::UNKNOWN:
#endif
    {
        float pitch, roll, dyaw, thrust;
        MyDroneMidLevelAutopilot.getDroneLLAutopilotCommand(&pitch, &roll, &dyaw, &thrust);

        droneMsgsROS::droneAutopilotCommand current_drone_autopilot_command_msg;
				current_drone_autopilot_command_msg.header.stamp = ros::Time::now();
        current_drone_autopilot_command_msg.pitch = pitch;
        current_drone_autopilot_command_msg.roll  = roll;
        current_drone_autopilot_command_msg.dyaw  = dyaw;
        current_drone_autopilot_command_msg.thrust= thrust;

        drone_autopilot_command_publisher.publish( current_drone_autopilot_command_msg );
        return true;
    } break;
    }
    return false;
}
